# Prelab for foray 99++ competition

## 1. Introduction:
The goal of this project is to investigate blimp systems and come up with an automatic design process. Generally, lighter-than-air vehicles (LTAVs) have the advantages of staying in the air for a very long time, and it is safer than drones due to its low speed and soft body. A LTAV can have different design parameters, such as the shape of its body, the configuration of its actuator, which would have an effect on its performance. It is hard to explore very different design parameters as it cost a large amount of time to modify, fabricate and test. This tool would empower the designer with the ability to explore different type of design of blimps in a very short amount of time. Some researchers have explored different types of LTAVs, however, none of them focuses on the autonomous design tool of such blimps. There are other researhers working on the design tool for other types of robots such as origami, which we could learn from them. 
What we propose is a robotic design tool for blimp that can provide real-time analysis of the design parameters of the blimp. The goal of this tool is that a design can use it to design a team of blimps which have good performance to win the competition. Therefoer, this can be validate by building a team of blimps that work synchronously with the goal of successfully playing a game of aerial soccer for approximately one hour. 
The competition in November does not requires full automation of controlling the blimps, which means we could spend more efforts on studying the relationship between the design parameters and performance. Therefore, the goal of this prelab is to raise some research questions that need to be answer during the competition.

## 2. Experiments
Here we summarize some experiments that could be conducted in the November competition.
### 2.1 The ability of localizing and ball recognizing using on-board camera
Despite the fact that all of the balloon will be tele-op in the November competition, which means sensors are not necessary on-borad, camera can still be a useful tool to help locate the position of the ball as it might be hard for human manipulator to precisely locate the position of the ball.

### 2.2 The ability of anti-interference of vertical control
Ideally, the blimps reaches its neutral buoyancy and can stay the same height without any control input. However, in real environment, there are different types of interference when controlling the blimps. For example, there could be vertical air flow in the warehouse, and there will be vertical velocity generated while controlling hthe blimps. Therefore, if we do not apply any control of the vertical input, the blimp will inevitably have a tendency to either move to the ceiling or to the ground. Adding a vertical motion controller could be a great solution. There are different sensors can be used for vertical control, for example:

#### 2.2.1 Environment-dependent sensors
Environment-dependent sensors include ultrasonic sensors, lidar, etc. These sensors send a signal to the environment and calculate the time-lapse when the signal bounce back from the environment. It's pretty reliable when there is no obstacle in the environment, however, the environment (warehouse) where our blimps will fly is full of other blimps, which means the signal sent from the sensors may be bounced back from other blimps, which could cause the data to be fause. Therefore, we choose not to use this type of sensors.

#### 2.2.1 Pressure-dependent sensors
Another type of height sensors uses the air-pressure to calculate the height such as altimeters, barometers. These sensors require calibration and the change of air pressure may affect the accuracy. The resolution of the height may also be large. As the warehouse is really large and high, we think we can use it for the vertical controller. 

## 3. Set-up
### 3.1 Essential equipment and electric components
- Fully operational blimp
- barometer sensors
- camera
