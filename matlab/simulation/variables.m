clc;clear;

% Basic specs
% Centor of mass
CoM = [0; 0; 1];
% Centor of Buoyancy
CoB = [0 0 0]';
% Gravity
M = 1; %kg
g = 9.81;
Gravity = M*g;
% F_B
rou = 1;
volume = 1;
F_B = rou*g*volume;

% Mass matrix
m11 = 1;
m22 = 1;
m33 = 1;
m44 = 1;
m55 = 1;
m66 = 1;

Mass = [m11 0 0 0 0 0;
        0 m22 0 0 0 0;
        0 0 m33 0 0 0;
        0 0 0 m44 0 0;
        0 0 0 0 m55 0;
        0 0 0 0 0 m66];
Mass_inv = inv(Mass);