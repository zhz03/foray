function volume = volume_cal(mass)
rho_helium = 0.1785; % density of helium in kg/m^3
rho_air = 1.225; % density of air in kg/m^3
volume = mass/(rho_air - rho_helium); % in m^3
end  