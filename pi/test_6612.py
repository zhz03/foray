#!/usr/bin/env python

# Import required modules
import time
import RPi.GPIO as GPIO

# Declare the GPIO settings
GPIO.setmode(GPIO.BOARD)

INA1 = 11
INA2 = 12
PWMA = 7

INB1 = 13
INB2 = 15
PWMB = 16

# Vertical
MODE = 18
EN = 19
PH = 21

GPIO.setup(INA1, GPIO.OUT)
GPIO.setup(INA2, GPIO.OUT)
GPIO.setup(PWMA, GPIO.OUT)
GPIO.setup(INB1, GPIO.OUT)
GPIO.setup(INB2, GPIO.OUT)
GPIO.setup(PWMB, GPIO.OUT)

GPIO.output(INA1, GPIO.LOW)
GPIO.output(INA2, GPIO.HIGH)

GPIO.output(INB1, GPIO.LOW)
GPIO.output(INB2, GPIO.HIGH)

pwm_A = GPIO.PWM(PWMA, 80)
pwm_A.start(0)

pwm_B = GPIO.PWM(PWMB, 80)
pwm_B.start(0)

GPIO.setup(MODE, GPIO.OUT) # Connected to AIN2
GPIO.output(MODE, GPIO.HIGH) # Set AIN1

GPIO.setup(EN, GPIO.OUT) # Connected to PWMA
GPIO.setup(PH, GPIO.OUT) # Connected to AIN1

GPIO.output(PH, GPIO.LOW) # Set AIN2

pwm_Ver = GPIO.PWM(EN, 80)
pwm_Ver.start(0)

try:
    while True:
        dutycycle_A = input('Enter a duty cycle percentage from 0-100 (A): ')
        print "Duty Cycle for A is : {0}%".format(dutycycle_A)
        pwm_A.ChangeDutyCycle(dutycycle_A)
        pwm_B.ChangeDutyCycle(dutycycle_A)
        pwm_Ver.ChangeDutyCycle(dutycycle_A)

        # dutycycle_B = input('Enter a duty cycle percentage from 0-100 (B): ')
        # print "Duty Cycle for B is : {0}%".format(dutycycle_B)
        # pwm_B.ChangeDutyCycle(dutycycle_B)

        # time.sleep(2)

except (KeyboardInterrupt, ValueError, Exception) as e:
    print(e)
    pwm_A.stop()     # stop the PWM output
    GPIO.cleanup() # clean up GPIO on CTRL+C exit