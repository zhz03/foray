#!/usr/bin/env python

# Import required modules
import time
import RPi.GPIO as GPIO

# Declare the GPIO settings
GPIO.setmode(GPIO.BOARD)

MODE = 7
EN_A = 13
PH_A = 15

EN_B = 11
PH_B = 12

GPIO.setup(MODE, GPIO.OUT) # Connected to AIN2
GPIO.output(MODE, GPIO.HIGH) # Set AIN1

# set up GPIO pins
GPIO.setup(EN_A, GPIO.OUT) # Connected to PWMA
GPIO.setup(PH_A, GPIO.OUT) # Connected to AIN1
# GPIO.setup(13, GPIO.OUT) # Connected to STBY
GPIO.setup(EN_B, GPIO.OUT) # Connected to PWMA
GPIO.setup(PH_B, GPIO.OUT)

# Drive the motor clockwise
GPIO.output(PH_A, GPIO.LOW) # Set AIN2
GPIO.output(PH_B, GPIO.LOW)

pwm_A = GPIO.PWM(EN_A, 200)
pwm_A.start(0)

pwm_B = GPIO.PWM(EN_B, 200)
pwm_B.start(0)


try:
    while True:
        dutycycle_A = input('Enter a duty cycle percentage from 0-100 (A): ')
        print "Duty Cycle for A is : {0}%".format(dutycycle_A)
        pwm_A.ChangeDutyCycle(dutycycle_A)
        time.sleep(1)
        pwm_B.ChangeDutyCycle(dutycycle_A)

        # dutycycle_B = input('Enter a duty cycle percentage from 0-100 (B): ')
        # print "Duty Cycle for B is : {0}%".format(dutycycle_B)
        # pwm_B.ChangeDutyCycle(dutycycle_B)

        # time.sleep(2)

except (KeyboardInterrupt, ValueError, Exception) as e:
    print(e)
    pwm_A.stop()     # stop the PWM output
    GPIO.cleanup() # clean up GPIO on CTRL+C exit

