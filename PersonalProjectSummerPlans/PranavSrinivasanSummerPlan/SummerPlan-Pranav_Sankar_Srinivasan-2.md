## UCLA LEMUR Summer Plan - Pranav Sankar Srinivasan


## Big Picture

This summer, I plan on working in LEMUR as a member of the Foray project. In this project, I will be working with my project members to create a fleet of blimps that are agile and fast with the goal of being capable of working together to play a game of air soccer. This fleet of blimps will be competing in a competition in November in Indiana. In addition, we will be working to create a design pipeline for blimp design and write a paper on this with the intentions of publishing it by submitting the paper in September to ICRA.

People should care about this problem because it is well known that blimps are the best types of UAVs when it comes to how long they can stay in the air due to their light weight. The problem with this is that blimps tend to be stationary or slow to move. If we can figure out a blimp design that mitigates these issues, then we will have come up with a UAV that can both sustain itself in the air for long durations of time as well as move freely in space which can potentially be applied in various different cases. In addition, developing a method with which these blimps can traverse space autonomously is relevant for the development of drone package delivery that companies such as Amazon are looking into.

Thus far, the most relevant research thus far has been done by a lab at Georgia Tech. They created a blimp that can move autonomously, detect specific objects and recognize certain commands, and complete predefined actions accordingly. This research will be very useful when we look into implementing object detection into our blimps as they need to be able to identify the ball in a soccer game but does not solve the issue of working with a team of blimps to determine movement. It also obviously does not know how to interface with a ball and maneuver it to a goal nor did they create a design pipeline. Finally, the design for their blimp was never done with agility and speed in mind which is contrary to what we plan to address. 


## Specific Project Scope

Within Foray, I plan on working with simulation software as it is vital for us to create simulations to test the various designs we sketch up because it saves time, money, and effort.

In addition, I also will be working to develop and implement the object detection software for the blimps so that they can be able to detect the ball and then execute predetermined commands which enable it to score the ball into a goal. 

Finally, once we have finalized our blimp design, I plan on investigating how to implement autonomy with which the blimps can use to move around. Two possible implementations include creating a network between the blimps that communicate with each other so that each blimp’s movement is based off of others. Another possibility is each blimp having the ability to traverse space and avoiding other objects using sensors that we install.

The subproblems are vital to the creation of a working fleet of blimps as without using simulations, implementing some amount of autonomy for movement, and enabling object detection.

In terms of simulations, I will do research on the different types of simulation software (specifically looking into what UAV simulation software currently exists) and look to use that to “sketch” out the different designs that my research partners and I can come up with. From there, I will run various relevant tests and determine which design best fits all our needs and also test to figure out how different elements affect different parameters of performance to further investigate how to develop a design pipeline.

For object detection, I will look into how the Georgia Tech researchers implemented object detection on their blimp and look into various other research papers and resources as well to determine the best way to integrate object detection.

Like I stated above, I will look into the two potential implementations of autonomous movement and determine which one is more suitable for our purposes. For the first concept, I will determine what will need to be done to enable robust network connectivity. If that doesn’t seem feasible, I will look into figuring out what sensors would be necessary to create a method for the blimps to move autonomously based on its surroundings. I will look to figure out how to create autonomous movement which could very easily be implemented via an Arduino which has preset commands based on what the sensors detect to be in the blimp’s surroundings.

This approach is designed so as to solve the most pressing problems first so that as a result, we ensure that a presentable product will be ready by the deadlines we have set.

The simulations will be rendered successful if I can help identify a design that satisfies as many of the criteria we create as best as possible within the time frame given to play around with simulations. We believe that we can definitely create a design that is faster than the design of the Amazon “floating shark” toys. Object detection will be considered a success if the software can detect an object at an 80% success rate. Autonomous movement is considered successful if the blimps can maneuver themselves around obstacles at an 80% success rate and execute appropriate commands if it detects the ball or any object it's trained to identify.



(Image 1 is discussed here)


We will need to engineer a method in which the blimp can discern between other blimps and the ball floating in the air. This can potentially be done using color detection and depth sensors.



(Image 2 discussed here)


The team A blimps have to be able to maneuver around other obstacles (such as the team B blimps as it tries to get to the ball. This can either be done by creating a network amongst the team A blimps or by installing sensors in all directions and moving based on where it detects the blimps to be.


## Broader Impact

Object detection software is very valuable for various applications of unmanned systems in general. Further exploring their applications for UAVs can help get society to get closer to completely autonomous drones for purposes such as package delivery and monitoring crops.

Implementing an object detection system will enable the blimps to detect the ball and actually be able to execute commands to attempt to score the ball and “play soccer.” Creating a method for which the blimps can traverse space while avoiding obstacles will reduce its chance of breaking and enable it to reach the ball at a more efficient pace.


## Background/ Related work/ References

N. Yao, E. Anaya, Q. Tao, S. Cho, H. Zheng, and F. Zhang. Monocular vision-based human following on miniature robotic blimp.  In 2017 IEEE International Conference on Robotics and Automation (ICRA), pages 3244–3249, 2017.

The source above is a research paper written by Georgia Tech researchers who developed a blimp that can detect specific objects and human hand motions. It is valuable to understand how they implemented their object detection software. 


#### Background / related work / references

Find external sources that support your framing of the research problem. In particular, you should establish your answers to all of the above questions using only material from other researchers.

You should also find additional sources that address:



*   What foundation and fundamentals need to be known in order to understand your problem, approach, and solution?
*   What work has been done before on this specific problem?
*   What are related problems that have been addressed, and what work has been done on those?
*   What are unrelated problems that have employed specific aspects of your proposed approach or solution?
*   How does this collection of past work contribute to your planned work? 
*   How do we know that your subproblem hasn't yet been solved?

Be sure to cite all potential sources, and summarize each one in terms of its content and relation to your project. 


## Goals, deliverables, tasks


### Goals

I hope to develop a successful object detection system. In addition, I hope to use simulation software to determine what the best design for the blimps should be. Finally, I plan on developing an efficient way for UAVs to efficiently traverse space either via a medium to communicate amongst each other or using sensors.

These goals will require knowledge in software development, integration of cameras and other sensors, and network communication.


### Deliverables and Tasks


#### Week 1

Aggregate components for the basic blimp. Determine method of implementing object detection software and start learning how to use relevant tools:


*   Create a Google Sheets with all relevant information for the parts that need to be ordered
*   Order parts
*   Research different implementations of object detection via research papers and other credible sources and compile information on one document
*   Determine which method will result the best balance of good results with least amount of time spent on object detection (because I have other tasks to complete in this project)
*   Order necessary hardware


#### Week 2

Work on the software implementation of the object detection



*   Learn the necessary software tools using tutorials and start working on our implementation of the object detection software


#### Week 3

Complete the software for object detection



*   Test the software to the best of my abilities (if the hardware is available I can completely test and finalize the software)
*   Software should be successful at least 70-80% of the time


#### Week 4

Start looking into best implementation of blimp navigation in space



*   Look up different methods of traversal of space that have been previously implemented for both UAVs and terrain robots
*   Determine which implementation is most effective and most efficient to implement
*   Order hardware


#### Week 5

Start implementing software for blimp navigation



*   Start learning the software needed using tutorials to implement the blimp navigation system
*   Start writing implementation for blimps


#### Week 6

Continue working on implementation of blimp navigation



*   Continue working on implementation for blimps


#### Week 7

Finalize and test software for blimp navigation



*   If hardware is available, test with that. Otherwise, test to the best of my abilities


#### Week 8

Start integrating all the aspects of the project



*   Incrementally add each aspect of the blimp together one at a time
*   Debug each component as it’s incrementally added
*   Test the final blimp design and start aggregating data for data pipeline via simulations et cetera
*   Test that blimp avoids other objects at least 70-80% of the time
*   Test that blimp traverses space to reach ball object in an optimal manner


#### Week 9

Continue the integration process



*   Integrate one component at a time as mentioned under the Week 8 deliverables and tasks section.
*   Continue developing the design pipeline and writing paper for publication


#### Week 10

Finalize the blimp design and create a poster board to present the project



*   Finish up research paper as well

There is going to be some leeway as to the timeline of the project as the competition is only in November but ideally, this is the timeline I will be following. A big variable in the timeline is the time it takes for my team members and I to receive the parts we order.
