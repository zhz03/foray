<!-- Copy and paste the converted output. -->


Ryan Chen

LEMUR 2020

**Summer Plan**

<span style="text-decoration:underline;">Introduction</span>

	This summer, I will be working on the FORAY project to create an armada of agile blimps. I’m very interested in the blimp design, and trying to balance speed, agility, and robustness. I have also done a small amount of work with Unity and its physics engine, so I also find the simulation and design part of the project very interesting as well. By the end of the summer, I hope to document and share my work with the series of blog posts at least once a week, as well as the ICRA publication.

<span style="text-decoration:underline;">Big Picture</span>

	The concept of lighter-than-air vehicles (LTAVs) is key for achieving long flight times with relatively low energy cost. However, this often comes at the cost of low speed and relatively immobile vehicles. Therefore, research in this field attempts to find novel ways of introducing agility and speed into LTAVs. For instance, Yue Wang et. al. worked on altitude control for indoor blimps. In addition, many projects in this field include low cost robots that interact with humans, which, in a sense, intersects with our goal of recognizing the ball in the air. Overall, I think this problem will become key in optimizing low-cost flight over long times and comparable agility and speed.

<span style="text-decoration:underline;">Specific Project Scope</span>

	Under the umbrella of designing an agile and fast blimp, our task includes creating a communicating armada of blimps as well. In order to solve this problem, we first will attempt to create an agile and well controlled blimp. In addition, we will be creating a mathematical model so that we can come up with a simulation or other design tools.

	Thus, the scope of the project is twofold. First, by the end of the summer, we hope to have a design pipeline for creating an optimized blimp, hopefully with some autonomous communication built in. Second, we hope to have a team of autonomous blimps ready for the competition later in the fall.

<span style="text-decoration:underline;">Broader Impact:</span>

<span style="text-decoration:underline;">	</span>Once we have achieved our solution for autonomous and agile blimps, the results of this project should be helpful in future endeavors involving the design of LTAVs. Eventually, blimps could become the safest and most feasible method of indoor flight, which could provide aid in many instances. For example, finding objects in a department store or examining hard-to-reach places could be a couple of the many applications of indoor blimps. In the future, people working with LTAVs will be able to look to the research conducted here for a strong method of designing blimps with exceptional functionality.

<span style="text-decoration:underline;">Weekly Plan:</span>

Week 1:



*   Goals
    *   Understand how current literature can help us achieve our goal with the project and codify it in a spreadsheet
*   Tasks
    *   Find literature relevant to the project
    *   Create a spreadsheet/document relating each piece of literature to a timeline and add comments about how they apply
*   Deliverables
    *   Blog post about key pieces of literature and why they’re useful, including my own/other cited graphics
    *   Completed Project Proposal including full list of weekly tasks

Week 2:



*   Goals
    *   A working model of the blimp in a design software like Unity or Blender
    *   Have a basic blimp design that can be used for preliminary testing
*   Tasks
    *   Use spreadsheet to list the necessary components along with their statistics i.e. weight and cost
    *   Model the design using software such as Unity or Blender, full visualized result
        *   I can create a unity/blender design once we have decided on one->offer to lead this section of the project in next group meeting on Monday
    *   Work with Pranav to see which camera we will be testing first
*   Deliverables
    *   Blog post including completed visual and why we chose the design we did
    *   Spreadsheet of components

Week 3:



*   Goals
    *   Determine the mobility of the blimp with a mathematical model of the design -> I have not worked on anything like this before, so I would need some help while working on this
    *   Prepare to compare the mathematical model with a hardware design
*   Tasks
    *   Create a basic controller
    *   (See week 5) Possibly buy necessary tools i.e. soldering iron
*   Deliverables
    *   Blog post including any mathematical models we have created and the design of the basic controller

Week 4:



*   Goals
    *   Full hardware design of blimp completed
*   Tasks
    *   I should be able to purchase any necessary components if they’re available on services like amazon for testing
*   Deliverables
    *   Blog post including any personal pictures of the physical design I have so far

Week 5:



*   Goals
    *   The team will validate the mathematical model: I have also not done this before, but I will help with any complementary tasks
    *   The blimp should be able to do fundamental maneuvering
    *   Preliminary/Rough Draft of ICRA Paper
*   Tasks
    *   I may need to order a soldering iron if I work on the control board, try to order by week 3 if necessary?
    *   Write up a rough draft of ICRA Paper in Overleaf and upload to git repository
*   Deliverables
    *   Blog post? Youtube video? Some interactive medium showing the maneuvering that we have achieved thus far.