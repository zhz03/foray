# Title:
Real-time analysis of blimp design

### Summary:
This project proposal for fall quarter aims to develop a design tool that is able to design the blimp autonomously while providing real-time analysis of the blimp performance while the designer is modifying the design.

### Splash images
![](/proposal_fall/fig/figure1.png)

### Project git repo(s):
https://git.uclalemur.com/ljhnick/foray

## Big picture 

### What is the overall problem that this and related research is trying to solve?
The overall problem to solve is: how to enable a designer of blimps to explore very different design of blimps very quickly.
### Why should people (everyone) care about the problem?
Generally, lighter-than-air vehicles (LTAVs) have the advantages of staying in the air for a very long time, and it is safer than drones due to its low speed and soft body. A LTAV can have different design parameters, such as the shape of its body, the configuration of its actuator, which would have an effect on its performance. It is hard to explore very different design parameters as it cost a large amount of time to modify, fabricate and test. This tool would empower the designer with the ability to explore different type of design of blimps in a very short amount of time.
### What has been done so far to address this problem?
Some researchers have explored different types of LTAVs, however, none of them focuses on the autonomous design tool of such blimps. There are other researhers working on the design tool for other types of robots such as origami, which we could learn from them.
## Specific project scope

### What subset of the overall big picture problem are you addressing in particular?
We plan to achieve real-time analysis of different design parameters while a designer is modifying them. The analysis result includes the speed, agility, payload, flight duration, etc.
### How does solving this subproblem lead towards solving the big picture problem?
picture is to solve the design problem of blimps by providing an autonomous design pipeline, it involves two different directions for solving the problem. The first is providing real-time analysis of different design parameters affecting the blimp performance and, based on that, the second is optimizing the design parameters based on designers’ goal. In this quarter, we will focus on the first part of the bigger picture.
### What is your specific approach to solving this subproblem?
We will mathematical model the relationship between the design parameters and the dynamic performance. The design parameters includes the shape of balloon, the configuration of motors and the payload.
### How can you be reasonably sure this approach will result in a solution?
A related paper about the computational design of multicopters uses a similar method toward the autonomous design of multicopters, which makes the method more reasonable.
### How will we know that this subproblem has been satisfactorily solved, using quantitative metrics?
We will conduct different tests of combining different design parameters and establish a simulation model and validate the results using physical experiments. 
## Broader impact
(even if someone doesn't care about the big picture problem that you started with, why should they still care about the specific work that you've produced?  Who else can use your processes and results, and how?)

### What is the value of your approach beyond this specific solution?
Such design pipeline can go beyond just LTAVs, but also for other types of robots, e.g., underwater robots or flying robots. 
### What is the value of this solution beyond solely solving this subproblem and getting us closer to solving the big picture problem?
Such mathematical modelling and optimization process can be applied to a more general design problem in robotics or other engineering areas.
## Background / related work / references
https://www.overleaf.com/read/krmmpvxnchqf

## Goals, deliverables, tasks

### Concrete external deadlines (paper submissions):
-IROS 2021 (Paper deadline: Late Feb.)

### Detailed schedule (weekly goals / deliverables / tasks):
Week    | gaols                         | deliverables                                                   | tasks                                         |
------- | ----------------------------- | -------------------------------------------------------------- | --------------------------------------------- |
Oct. 9  | achieve control of multiple blimps with only one person | a solution that a person could easily switch between control of different blimps | coding in arduino and using blynk app |
Oct. 16 | generate a strategy of controlling blimps whose number is double the number of controller / finish the design of capturing mechanism | 2 people could control at least four blimps to collaborately capture a ball and send it to the hoop | designing a sticking based capturing mechanism  |
Oct. 23 | improve the design | better version of the blimp team | assemble and fabricate multiple blimps as a fleet                    |
Oct. 30 | practice controlling the team        | two people control two different teams that could compete in the lab | practicing |
Nov. 6  | be ready for the competition         | be ready for the competition  | be ready for the competition |
Nov. 20 | mathematically model the blimp       | a mathematical model | model the shape of balloon, motor configuration |
Dec. 4  | mathematically model the blimp       | a test that could quantitatively validate the modeled result |   physical test      |

Oct. 12 - Oct. 14 | combine flap and motor, run test of controlling the blimp to touch the wall in the apartment | change the wiring system to wire wrap

Oct. 15 - 16 | compare between different configuration, determine the best config |fabricate defender blimp (stacking two sphere balloon) | test balloon capturing mechanism

Oct. 19 - 20 | debug and improve the design of defender blimp

Oct. 21 | set up the wifi connection of the microcontroller using an individual router | set up test environment in the lab (36'' hoola hoop / 24 '' ball)

Oct. 22 - 23 | test in the lab (two commander, each control a team of three blimps, one defender, two attacker.)

Oct. 26 - 30 | simulating the competition in the lab | debugging the design

Nov. 2 - Competition data | practice and iterate the design 

