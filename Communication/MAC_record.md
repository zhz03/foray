Zhaoliang:

| Num                 | MAC               |
| ------------------- | ----------------- |
| NodeMCU_esp8266_001 | 48:3F:DA:0C:E0:AE |
| NodeMCU_esp32_001   | 8C:AA:B5:8C:A4:4C |
| NodeMCU_N01         | 48:3F:DA:0C:DA:A9 |
| NodeMCU_N02         | 48:3F:DA:0C:E1:4D |
| NodeMCU_N03         | 48:3F:DA:0C:D7:98 |
| NodeMCU_N04         | 48:3F:DA:0C:DF:83 |
| NodeMCU_N05         | 48:3F:DA:0C:D3:6C |
| NodeMCU_N06         | 40:F5:20:29:D9:0D |
| NodeMCU_N07         | 10:52:1C:ED:22:F7 |
| NodeMCU_N08         | 48:3F:DA:7E:52:0C |
| NodeMCU_N09         | 48:3F:DA:0C:D5:4A |
| NodeMCU_N10         | 48:3F:DA:7E:0F:30 |

