#include <esp_now.h>
#include <WiFi.h>

int AIN1 = 25;
int AIN2 = 33;
int BIN1 = 26;
int BIN2 = 27;
int PWMA = 4;
int PWMB = 0;
int STNDBY = 32;
int SpeedHV = 0;

// Structure example to receive data
// Must match the sender structure
typedef struct struct_message {
    int x;
    int y;
} struct_message;

// Create a struct_message called myData
struct_message myData;

// callback function that will be executed when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&myData, incomingData, sizeof(myData));
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.println(myData.x);
  Serial.println(myData.y);

 if (myData.x == 1 && myData.y == 2)
 {
  Serial.println("back");
  move_backward();
 }
 else if(myData.x == 1 && myData.y == 1)
 {
  Serial.println("stop");
  stopall();
 }
 else if(myData.x == 1 && myData.y == 0)
 {
  Serial.println("forward");
  move_forward();
 }
 else if(myData.x == 0 && myData.y == 1)
 {
  Serial.println("left");
  move_left();
 }
 else if(myData.x == 2 && myData.y == 1)
 {
  Serial.println("right");
  move_right();
 }
}
 
void setup() {
  // Initialize Serial Monitor
  Serial.begin(115200);
  pinMode (AIN1,OUTPUT);
  pinMode (AIN2,OUTPUT);
  pinMode (BIN1,OUTPUT);
  pinMode (BIN2,OUTPUT);
  pinMode (PWMA,OUTPUT);
  pinMode (PWMB,OUTPUT);
  pinMode (STNDBY,OUTPUT);
  digitalWrite(STNDBY,HIGH);
  digitalWrite(PWMA,HIGH);
  digitalWrite(PWMB,HIGH);
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);
}
 
void loop() {

}

void move_right()
{
  digitalWrite(AIN1,HIGH);
  digitalWrite(AIN2,LOW);
  digitalWrite(BIN1,HIGH);
  digitalWrite(BIN2,LOW);
}
void move_left()
{
  digitalWrite(AIN1,LOW);
  digitalWrite(AIN2,HIGH);
  digitalWrite(BIN1,LOW);
  digitalWrite(BIN2,HIGH);
}
void move_backward()
{
  digitalWrite(AIN1,LOW);
  digitalWrite(AIN2,HIGH);
  digitalWrite(BIN1,HIGH);
  digitalWrite(BIN2,LOW);
}
void move_forward()
{
  digitalWrite(AIN1,HIGH);
  digitalWrite(AIN2,LOW);
  digitalWrite(BIN1,LOW);
  digitalWrite(BIN2,HIGH);
}
void stopall()
{
  digitalWrite(AIN1,LOW);
  digitalWrite(AIN2,LOW);
  digitalWrite(BIN1,LOW);
  digitalWrite(BIN2,LOW);
}
