# Send Keyboard Characters over Serial Port
# The Code is for Windows Only
# Download the necessary libraries - pyserial and msvcrt
# Change the PORT
# USAGE: python keyboard.py
#Type a character to send it to the port via serial communication

import msvcrt
import serial
serial_port = serial.Serial("COM6",115200)
serial_port.close()
serial_port.open()
input_char = msvcrt.getch().decode("utf-8")
while(input_char != 'q'):
	print(input_char)
	t = serial_port.write(input_char.encode('utf-8'))
	input_char = msvcrt.getch().decode("utf-8")
	
	
