# How to manually control multiple NodeMCU using Blynk App

To control multiple devices on one cellphone. We need to:

- Setup blynk local server
- Setup your Blynk interface
- Upload corresponding code to NodeMCU

## Setup Blynk local server

### Reasons for using Blynk local server

The reason why we need to set up Blynk local server is because that if we were to use Blynk server, we only have 2000 "energy balance" to deploy our widgets (Joystick for example). And Device Selector takes about 1900 "energy", which leaves us no energy to deploy other widgets. You have to pay for more energy. 

The following picture shows what happens after we deploy 2 joysticks and 2 slider:

![blynk_server1](figures/blynk_server1.png?resize=350)

Except for money, local server also allows us to have the full control of everything. And it's easy to do, so why not? 

### How to build your own Blynk local server

The tutorial is here: [https://github.com/blynkkk/blynk-server](https://github.com/blynkkk/blynk-server)

To save you some times, I'll simplify this process into 3 steps:

- No matter what OS you are using (Linux, Windows, Mac), you should download and install Java 11 or Java 8 first before you start your Blynk local server. 

- After you finish installing Java, check your Java version using following command:

  ```
  Java -version
  ```

- Download latest server build [here](https://github.com/blynkkk/blynk-server/releases) (Corresponding Java11 or Java 8 version)

- In the folder that you download blynk server, type the following command to start your blynk local server:

  - Java 11

  ```
  java -jar server-0.41.13.jar -dataFolder /path
  ```

  - Java 8

  ```
  java -jar server-0.41.13-java8.jar -dataFolder /path
  ```

  That's it!

After you start your Blynk local server, you should see the following words or similar words:

```
Blynk Server 0.41.14-SNAPSHOT successfully started.
```

It means that you're good to go!

## Setup Blynk interface

After you successfully started your local server, you should check your server IP address using:

```
ipconfig # for windows
ifconfig # for mac and linux
```

Remember your IPV4 Address, we will need that later.

Open your Blynk app, and follow the steps in the following figures:

![blynk_server_comb1](figures/blynk_server_comb1.jpg)

![blynk_server_comb1](figures/blynk_server_comb2.jpg)

After you finish setting up your blynk app, don't forget to copy the "AUTH TOKENS" in the setting of each one of your project:

![blynk_server_comb1](figures/blynk_server_comb3.jpg)

## Upload corresponding code to NodeMCU

I uploaded an example code here

To control different devices, you need to change the AUTH TOKENS from the code to the one you copy from your Blynk app, which is:

```
char auth[] = "ivazO8s9spBTyUeNTaBHFTsBaaI0CmRY";
# change the above code to yours
char auth[] = "your tokens";
```

