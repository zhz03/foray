# Manual Control of Blimp

## Content

[0 What is blimp](0 What is blimp)

[1 What is manual control](1 What is manual control)

[2 Hardware](2 Hardware)

[3 WiFi Communication](WiFi Communication)

## 0 What is blimp

Blimp is a lighter air vehicle, a non-rigid airship that could hover and move in the air. Unlike quadcopter, blimp utilizes buoyance to hover in the air.   

## 1 What is manual control

A manual control in the control theory is actually call *open-loop control*. To be more specific, the open-loop control is to have input signals (Your operation on RC controller or on cellphone) from user and to output signals to the corresponding actuators(The motors). 

To navigate your manual controlled blimp, You need to utilize your senses, eyes for example,  to locate the blimp. 

In this paper, we are going to mainly talk about manual control of blimp and its corresponding hardware and program. 

## 2 Hardware 

### Control board -- NodeMCU

- Real product photo show:

  ![NodeMCU](figures/NodeMCU.jpg)	

- Weight (each with headers): $7.5g \pm 0.5g$

- Communication method: Wifi

- For more information, please check: https://lastminuteengineers.com/esp8266-nodemcu-arduino-tutorial/

### Motor driver -- TB6612FNG

- Real product photo show:

  ![tb6612fng](figures/tb6612fng.jpg)

- Weight (each with headers): $3g \pm 0.3g$

### Motors + Propellers

- Real product photo show:

  ![motorpropeller](figures/motorpropeller.png)

- Weight (each pair): $6.5g \pm 0.5g$

### Battery -- Lipo 3.7v 1000mAh

- Real product photo show:

   ![LipoBattery1000](figures\LipoBattery1000.jpg)

### Hardware connection

-  Circuit diagram:

  ![manualCtl_NodeMCU](circuit_plot_doc/manualCtl_NodeMCU.png)

## 3 WiFi Communication

### Requirements

- Install **Esp8266WiFi** Library in Arduino IDE
- Install **Blynk** Library in Arduino IDE
- Download **Blynk** app in *App store* or *Play store* on your iPhone or Android devices.
- Your devices (ex. NodeMCU) and your cellphone to control that device should be in the same local area network(LAN)

### Tutorial

#### Step 1: set up your cellphone Blynk app

![step4_1](figures/step1_fig1.jpg)

![step4_1](figures/step1_fig2.jpg)

* Go to the playstore and download Blynk app. The welcome interface is shown in the figure a.
* After that, go to the *create new account*  and create your own blynk account. 
* To use blynk company server, in the server setting, chose blynk option 
* When you log in, you will see figure d
* Click *New project* to create a new project. And you'll see figure e
* Click *add* icon to add new module to your project, just like figure f
* After you finish creating your new module, click *Project settings* to send your **AUTH TOKENS** 

#### Step 2: set up your local Blynk server

Local Blynk server is an open-source Java based server. It's easy to build, and it gives unlimited energy balance on your cellphone application.

Here is the latest local blynk server download [link](https://github.com/blynkkk/blynk-server/releases).

You can check more details about installation and other instruction on Github: https://github.com/blynkkk/blynk-server#getting-started

#### Step 3: Upload your program to Control Board

To utilize blynk to communicate with control board, you need to generate your authorization tokens from Blynk app. And copy paste that tokens to your program. 

An example program to test your connection: 

```C
#define BLYNK_PRINT Serial 
#include <ESP8266WiFi.h> 
#include <BlynkSimpleEsp8266.h> 
// You should get Auth Token in the Blynk App. 
// Go to the Project Settings (nut icon). 
char auth[] = "Your AUTH TOKENS"; 
// Your WiFi credentials. 
// Set password to "" for open networks. 
char ssid[] = "Your wifi name"; 
char pass[] = "Your wifi password"; 


void setup(){ 
 // Debug console 
 Serial.begin(9600); 
  Blynk.begin(auth, ssid, pass,IPAddress(192,xxx,xx,xx),8080); // change IPAddress to your laptop IP address that runs your local blynk server

} 

void loop() {
  // put your main code here, to run repeatedly:
  Blynk.run();
}

BLYNK_WRITE(V1){
  int x = param[0].asInt();
  int y = param[1].asInt();  
  if(x!=0 && y!=0){
  Serial.print(x);
  Serial.print(y);
  Serial.println();
  delay(100);}
}

```

Upload this program to your Nodemcu.

#### Step 4: Create corresponding control icon on your Blynk app

We will use our code in step 3 as an example to show how to create a corresponding icon on the Blynk app. The "magic code" we put into BLYNK_WRITE is actually to take inputs from a Joystick. You can use other icon as well. There are corresponding example code in the Blynk app. Detailed operation is as follow:

- Use the **"plus"** icon $+$ at the top right to create a **JOYSTICK**

![step4_1](figures/step4_1.jpg?resize=300)

- Click that JOYSTICK icon in your project, you will go into the icon settings. Change **SPLIT** to **MERGE**

  ![step4_2](figures/step4_2.jpg?resize=300)

- Change **PIN** to **Virtual - V1**

  ![step4_3](figures/step4_3.jpg?resize=300)

- Change the input range from $0 \sim 1024$ to $-1 \sim 1$

  ![step4_4](figures/step4_4.jpg?resize=300)

#### Step 5: Test your connection

Power up your NodeMCU, make sure your code is up and running. Run your Blynk project, which is the $\rhd$ at the top right corner. 

You can operate your joystick and see if there is anything shows up in your Arduino IDE serial monitor. 

## 4 My example code

The horizontal and vertical speed control code: [NodeMCU code](code/NodeMCU_8_HorVerCtl/NodeMCU_8_HorVerCtl.ino)

