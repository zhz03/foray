# NodeMCU control motors using DRV8835

## What is DRV8835

DRV8835 is a tiny dual H-bridge motor driver IC that can be used for bidirectional control of two brushed DC motors at 0 V to 11 V.

The link: https://www.pololu.com/product/2135

The weight of DRV8835 < 1 g

## Wiring 

![NodeMCU_DRV8835](circuit_plot_doc/NodeMCU_DRV8835.png)

## Code

The source code is in [NodeMCU_9_testDRV8835.ino](Code/NodeMCU_9_testDRV8835/NodeMCU_9_testDRV8835.ino)

