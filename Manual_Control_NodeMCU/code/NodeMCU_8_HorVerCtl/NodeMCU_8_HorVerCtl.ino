#define BLYNK_PRINT Serial 
#include <ESP8266WiFi.h> 
#include <BlynkSimpleEsp8266.h> 
// You should get Auth Token in the Blynk App. 
// Go to the Project Settings (nut icon). 
char auth[] = "Your auth tokens"; 
// Your WiFi credentials. 
// Set password to "" for open networks. 
char ssid[] = "Your wifi name"; 
char pass[] = "Your password"; 

int Right1=16;// D0 
int Right2=5;// D1 
int PWMH = 4; // D2 
int Left1=0;// D3
int Left2=2;// D4 

int up1 = 14; // D5
int down1 = 12; // D6
//int PWMV = 13; // D7
int up2 = 13; // D7
int down2 = 15; // D8

// -------- actuator functions ------//
void Horizontal_move(boolean a1,boolean a2,boolean b1,boolean b2){
  digitalWrite(Right1,a1);
  digitalWrite(Right2,a2);
  digitalWrite(Left1,b1);
  digitalWrite(Left2,b2);
}

void vertical_move(boolean a1,boolean a2,boolean b1,boolean b2){
  digitalWrite(up1,a1);
  digitalWrite(down1,a2);
  digitalWrite(up2,b1);
  digitalWrite(down2,b2);
}

void up(){
  vertical_move(HIGH,LOW,HIGH,LOW);
}
void down(){
  vertical_move(LOW,HIGH,LOW,HIGH);
}
void stop_updown(){
  vertical_move(LOW,LOW,LOW,LOW);
}

void forward(){
  Horizontal_move(LOW,HIGH,LOW,HIGH);
}
void backward(){
  digitalWrite(Right1,HIGH);
  digitalWrite(Right2,LOW);
  digitalWrite(Left1,HIGH);
  digitalWrite(Left2,LOW);
  }
void stopall(){
  digitalWrite(Right1,LOW);
  digitalWrite(Right2,LOW);
    digitalWrite(Left1,LOW);
  digitalWrite(Left2,LOW);  
}
void turn_left(){
  digitalWrite(Right2,HIGH);
  digitalWrite(Right1,LOW);
  digitalWrite(Left1,LOW);
  digitalWrite(Left2,LOW); 
}
void turn_right(){
  digitalWrite(Right1,LOW);
  digitalWrite(Right2,LOW);
  digitalWrite(Left1,LOW);
  digitalWrite(Left2,HIGH);
 
}

void setup(){ 
 // Debug console 
 Serial.begin(9600); 
  Blynk.begin(auth, ssid, pass); 
 pinMode(Right1, OUTPUT); 
 pinMode(Right2, OUTPUT); 
 pinMode(Left1, OUTPUT); 
 pinMode(Left2, OUTPUT);
 pinMode(PWMH, OUTPUT); 
 //pinMode(PWMV, OUTPUT); 
 pinMode(up1, OUTPUT); 
 pinMode(up2, OUTPUT); 
 pinMode(down1, OUTPUT); 
 pinMode(down2, OUTPUT); 
} 

void loop(){ 
Blynk.run();
}
 
BLYNK_WRITE(V1){
  int x = param[0].asInt();
  int y = param[1].asInt();  
 if (x == 0&& y==-1){
  backward();
 }else if(x == 0&& y==0){
  stopall();
 }else if(x == 0&& y==1){
  forward();
 }else if(x==-1&&y==0){
  turn_left();
 }else if(x==1&&y==0){
  turn_right();
 }
}

BLYNK_WRITE(V2){
  int x = param[0].asInt();
  int y = param[1].asInt();
  if(x==0 && y==1){
     up(); 
  }else if(x==0 && y==-1){
    down();
  }else if(x==0 && y==0){
    stop_updown();
  }
}

BLYNK_WRITE(V0){
  int SpeedHV = param.asInt();
  analogWrite(PWMH,SpeedHV);
  //analogWrite(PWMV,SpeedHV);
}
