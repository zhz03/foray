#define BLYNK_PRINT Serial 
#include <ESP8266WiFi.h> 
#include <BlynkSimpleEsp8266.h> 
char auth[] = "PAkKZ43aBs_tow2P1x5SIbav9gE4Ef_Q"; 
// Your WiFi credentials. 
// Set password to "" for open networks. 
char ssid[] = "NETGEAR89"; 
char pass[] = "huskyplum599";

int d0   = 16;
int d1   = 5;
int d2   = 4;
int d3   = 0;
int d4   = 2;
int d5   = 14;
int d6   = 12;
int d7   = 13;
int d8   = 15;
int d9 = 3;
int d10 = 1;
int SpeedHV = 0;

void Horizontal_move(boolean a1,boolean a2,boolean b1,boolean b2){
  digitalWrite(d5,a1);
  digitalWrite(d6,a2);
  digitalWrite(d9,b1);
  digitalWrite(d10,b2);
}

void stopall(){
  Horizontal_move(LOW,LOW,LOW,LOW);
}

void forward(){
  Horizontal_move(LOW,HIGH,LOW,HIGH);
}

void backward(){
  Horizontal_move(HIGH,LOW,HIGH,LOW);
}

void turn_right(){
  Horizontal_move(LOW,HIGH,LOW,LOW);
}

void turn_left(){
  Horizontal_move(LOW,LOW,LOW,HIGH);
}

void vertical_move(boolean a1,boolean a2){
  digitalWrite(d7,a1);
  digitalWrite(d8,a2);
}

void up(){
  vertical_move(HIGH,LOW);
}
void down(){
  vertical_move(LOW,HIGH);
}
void stop_ver(){
  vertical_move(LOW,LOW);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); 
  //Blynk.begin(auth, ssid, pass,IPAddress(192,168,0,18),8080); 
  Blynk.begin(auth, ssid, pass);
  pinMode(d5,OUTPUT);
  pinMode(d6,OUTPUT);
  pinMode(d7,OUTPUT);
  pinMode(d8,OUTPUT);
    
  pinMode(d9,FUNCTION_3);
  pinMode(d10,FUNCTION_3);
  pinMode(d9,OUTPUT);
  pinMode(d10,OUTPUT);
  pinMode(d4,OUTPUT);   // pwm
  
}

void loop() {
  // put your main code here, to run repeatedly:
Blynk.run();
}

BLYNK_WRITE(V1){
  int x = param[0].asInt();
  int y = param[1].asInt();  
 if (x == 0&& y==-1){
  backward();
 }else if(x == 0&& y==0){
  stopall();
 }else if(x == 0&& y==1){
  forward();
 }else if(x==-1&&y==0){
  turn_left();
 }else if(x==1&&y==0){
  turn_right();
 }
}

BLYNK_WRITE(V0){
  SpeedHV = param.asInt();
  analogWrite(d4,SpeedHV);
  //analogWrite(PWMV,SpeedHV);
}


BLYNK_WRITE(V3){
  int x = param[0].asInt();
  int y = param[1].asInt();  
 if (x == 0&& y==0){
  stop_ver();
 }else if(x == 1&& y==0){
  up();
 }else if(x == -1&& y==0){
  down();
 }
}
