#define BLYNK_PRINT Serial 
#include <ESP8266WiFi.h> 
#include <Servo.h>
#include <BlynkSimpleEsp8266.h>
char auth[] = "9IekNa4U7fGWYen3HuTZB4qi4p5GuiVc"; 
//char auth[] = "1b3YByQeL5GGHe-u46xuN7jOS-Q1LHso"; 
char ssid[] = "NETGEAR89"; 
char pass[] = "huskyplum599"; 
Servo myservo;

int d0   = 16;
int d1   = 5;
int d2   = 4;
int d3   = 0;
int d4   = 2;
int d5   = 14;
int d6   = 12;
int d7   = 13;
int d8   = 15;
int d9 = 3;
int d10 = 1;

int SpeedH = 0;
int SpeedV = 0;

int x_1 = 0;
int y_1 = 0;
int pos;

void Horizontal_move(boolean a1,boolean a2,boolean b1,boolean b2){
  digitalWrite(d5,a1);
  digitalWrite(d6,a2);
  digitalWrite(d7,b1);
  digitalWrite(d8,b2);
}

void stopall(){
  Horizontal_move(LOW,LOW,LOW,LOW);
}

void forward(){
  Horizontal_move(LOW,HIGH,LOW,HIGH);
}

void backward(){
  Horizontal_move(HIGH,LOW,HIGH,LOW);
}

void turn_right(){
  Horizontal_move(LOW,HIGH,LOW,LOW);
}

void turn_left(){
  Horizontal_move(LOW,LOW,LOW,HIGH);
}

void vertical_move(boolean a1,boolean a2){
  digitalWrite(d9,a1);
  digitalWrite(d10,a2);
}

void up(){
  vertical_move(HIGH,LOW);
}
void down(){
  vertical_move(LOW,HIGH);
}
void stop_ver(){
  vertical_move(LOW,LOW);
}

void setup() {
  // put your setup code here, to run once:
 Serial.begin(9600); 
 //Blynk.begin(auth, ssid, pass); 
 Blynk.begin(auth, ssid, pass,IPAddress(192,168,0,16),8080); 
  pinMode(d5,OUTPUT);
  pinMode(d6,OUTPUT);
  pinMode(d7,OUTPUT);
  pinMode(d8,OUTPUT);
  
  pinMode(d9,FUNCTION_3);
  pinMode(d10,FUNCTION_3);
  pinMode(d9,OUTPUT);
  pinMode(d10,OUTPUT);
  
  pinMode(d3,OUTPUT);

  myservo.attach(d0);
}

void loop() {
  // put your main code here, to run repeatedly:
 Blynk.run();
}

BLYNK_WRITE(V2){
  x_1 = param.asInt();
}

BLYNK_WRITE(V3){
  y_1 = param.asInt();
}
 
BLYNK_WRITE(V0){
  SpeedH = param.asInt();
  analogWrite(d3,SpeedH);
   
}

BLYNK_WRITE(V1){
  SpeedV = param.asInt();
  analogWrite(d4,SpeedV);
  Serial.println(SpeedV); 
}

BLYNK_WRITE(V4){
  int x = param[0].asInt();
  int y = param[1].asInt();  
    Serial.print(x);
  Serial.println(y); 
 if (x == 0&& y==0){
  stop_ver();
 }else if(x == 0&& y==1){
  digitalWrite(d9,HIGH);
  digitalWrite(d10,LOW);
 }else if(x == 0&& y==-1){
  digitalWrite(d9,LOW);
  digitalWrite(d10,HIGH);
 }
}

BLYNK_WRITE(V5){
  pos = param.asInt();
  myservo.write(pos);
  delay(15);
}
