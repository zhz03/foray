// 4 motors split 

#define BLYNK_PRINT Serial 
#include <ESP8266WiFi.h> 
#include <BlynkSimpleEsp8266.h>
char auth[] = "e15CFApgfigqa2N12wQp3NiaDbQZ7RYU";

//char auth[] = "203HLv48dHjFEP5CPXm5j0WLrIifGGzE";
// char auth[] = "1b3YByQeL5GGHe-u46xuN7jOS-Q1LHso"; 
char ssid[] = "NETGEAR89"; 
char pass[] = "huskyplum599"; 

int d0   = 16;
int d1   = 5;
int d2   = 4;
int d3   = 0;
int d4   = 2;
int d5   = 14;
int d6   = 12;
int d7   = 13;
int d8   = 15;
int d9 = 3;
int d10 = 1;
int SpeedH = 0;
int SpeedV = 0;

int x_1 = 0;
int y_1 = 0;

void Horizontal_move(boolean a1,boolean a2,boolean b1,boolean b2){
  digitalWrite(d5,a1);
  digitalWrite(d6,a2);
  digitalWrite(d7,b1);
  digitalWrite(d8,b2);
}

void stopall(){
  Horizontal_move(LOW,LOW,LOW,LOW);
}

void forward(){
  Horizontal_move(LOW,HIGH,LOW,HIGH);
}

void backward(){
  Horizontal_move(HIGH,LOW,HIGH,LOW);
}

void turn_right(){
  Horizontal_move(LOW,HIGH,LOW,LOW);
}

void turn_left(){
  Horizontal_move(LOW,LOW,LOW,HIGH);
}

void vertical_move(boolean a1,boolean a2){
  digitalWrite(d9,a1);
  digitalWrite(d10,a2);
}

void up(){
  vertical_move(HIGH,LOW);
}
void down(){
  vertical_move(LOW,HIGH);
}

void stop_ver(){
  vertical_move(LOW,LOW);
}

void setup() {
  // put your setup code here, to run once:
 Serial.begin(9600); 
 //Blynk.begin(auth, ssid, pass); 
 Blynk.begin(auth, ssid, pass,IPAddress(192,168,0,16),8080); 
  pinMode(d5,OUTPUT);  //AIN2
  pinMode(d6,OUTPUT); // AIN1
  pinMode(d7,OUTPUT); //BIN2
  pinMode(d8,OUTPUT); // BIN1
  
  pinMode(d9,FUNCTION_3);
  pinMode(d10,FUNCTION_3);
  pinMode(d9,OUTPUT); // AIN2
  pinMode(d10,OUTPUT); //AIN1
  
  pinMode(d3,OUTPUT); // pwm
  pinMode(d4,OUTPUT); //pwm

}

void loop() {
  // put your main code here, to run repeatedly:
 Blynk.run();
 
}

BLYNK_WRITE(V2){
  x_1 = param[0].asInt();
  y_1 = param[1].asInt();
   if (x_1 == 0&& y_1==-1){
  backward();
 }else if(x_1 == 0&& y_1==0){
  stopall();
 }else if(x_1 == 0&& y_1==1){
  forward();
 }else if(x_1==-1&&y_1==0){
  turn_left();
 }else if(x_1==1&&y_1==0){
  turn_right();
 }
}

BLYNK_WRITE(V0){
  SpeedH = param.asInt();
  analogWrite(d3,SpeedH);  
}

BLYNK_WRITE(V1){
  SpeedV = param.asInt();
  analogWrite(d4,SpeedV);  
}


BLYNK_WRITE(V3){
  int x_2 = param[0].asInt();
  int y_2 = param[1].asInt();  

 if (x_2 == 0&& y_2 ==0){
  stop_ver();
 }else if(x_2 == 0&& y_2 ==1){
 up();
 }else if(x_2 == 0&& y_2 ==-1){
down();
 }
}
