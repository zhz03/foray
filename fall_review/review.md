# Grad student / advisor review

- Name: Jiahao (Nick) Li
- Term: Fall 2020

## Summary of recent progress / efforts
Built a fully functional blimp that can be manual controlled by human.
Explored different design parameters (motor configuration)


## Student requests for advisor action
Would need the resources in the lab such as the opti-track motion capturing system when investigating the effect of different design parameters.


## Advisor suggestions to student



## Progress towards long term goals
Working toward the third fabrication / robotics related paper during the Ph.D.


## Concrete upcoming goals and milestones
Early November: a team of blimps that could play aerial soccer
Late Feburary 2021: a research paper submitted to IROS 2021